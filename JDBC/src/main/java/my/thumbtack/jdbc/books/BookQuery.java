package my.thumbtack.jdbc.books;

import java.sql.*;
//
public class BookQuery {

    public static void getBooks(Connection con, String query) {
        try (Statement stmt = con.createStatement(); ResultSet rs = stmt.executeQuery(query)) {
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("title");
                Integer year = rs.getInt("year");
                Integer pages = rs.getInt("pages");
                Book book = new Book(id, title, year, pages);
                System.out.println(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void executeBooksQuery(Connection con, String query) {
        int result;
        try (Statement stmt = con.createStatement()) {
            result = stmt.executeUpdate(query);
            System.out.println("Result = " + result);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
