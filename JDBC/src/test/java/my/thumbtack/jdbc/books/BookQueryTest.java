package my.thumbtack.jdbc.books;

import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static org.junit.Assert.fail;

public class BookQueryTest {

    private static final String URL = "jdbc:mysql://localhost:3306/bookstore";
    private static final String USER = "root";
    private static final String PASSWORD = "62492381";

    private static boolean loadDriver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            System.out.println("Error loading JDBC Driver ");
            e.printStackTrace();
            return false;
        }
    }

    @Test
    public void test() {
        if (!loadDriver())
            return;
        String selectQuery = "select * from books";
        String selectWithLimitQuery = "select * from books limit 3";
        String selectWithID = "select * from books where id = 3";
        String selectWithTitleStart = "select * from books where title like \"w%\"";
        String selectWithTitleEnd = "select * from books where title like \"%w\"";
        String selectWithTitle = "select * from books where title like \"book1\"";
        String selectBetweenYears = "select * from books where year between 2010 and 2015";
        String selectBetweenPages = "select * from books where pages between 2 and 10";
        String selectBetweenPagesAndBetweenYears = "select * from books where year between 2015 and 2017 and pages between 10 and 500";
        String setPagesTo100 = "update books set pages = 100";
        String setPagesIfPages = "update books set pages = 52 where pages > 99";
        String setPagesIfPagesAndYear = "update books set pages = 64 where pages > 20 and year < 2016";
        String setPagesIfPagesAndTitle = "update books set pages = 72 where pages > 60 and title like \"w%\"";
        String setPublishingHouse = "update books set publishing_house = \"omsk\"";
        String deleteIfPages = "delete from books where pages > 100";
        String deleteWithTitleStart = "delete from books where title like \"w%\"";
        String deleteBetweenId = "delete from books where id between 3 and 6";
        String deleteWithTitle = "delete from books where title like \"book4\"";
        String deleteWithTitles = "delete from books where title like \"book1\" or title like \"book2\" or title like \"book3\"";
        String deleteAll = "delete from books";
        String createBindingTable = "drop table if exists binding;\n CREATE TABLE binding (\n" +
                "  id INT(11) NOT NULL AUTO_INCREMENT,\n" +
                "  name VARCHAR(50) NOT NULL,\n" +
                "  PRIMARY KEY (id),\n" +
                "  KEY name(name)\n" +
                ");\n" +
                "INSERT INTO binding VALUES(NULL, \"none\");\n" +
                "INSERT INTO binding VALUES(NULL, \"soft\");\n" +
                "INSERT INTO binding VALUES(NULL, \"hard\");";
        String addPublishingHouseColumnToBooks = "alter table books add column publishing_house varchar(50)";
        String addBindingColumnToBooks = "alter table books add column binding varchar(50) not null default 'none';\n" +
                "alter table books add foreign key (binding) references binding (name);";
        String renameTitleColumn = "alter table books change column `title` `bookname` varchar(50) not null";
        String renameTableColumn = "rename table books to BOOKS";
        String dropTable = "drop table BOOKS";
        try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD)) {
            BookQuery.getBooks(con, selectQuery);
            BookQuery.executeBooksQuery(con, setPagesTo100);
            BookQuery.executeBooksQuery(con, deleteBetweenId);
            BookQuery.executeBooksQuery(con, createBindingTable);
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
            fail();
        }
    }
}