alter table books add column publishing_house varchar(50);
alter table books add column binding varchar(50) not null default 'none';
alter table books add foreign key (binding) references binding (name);
alter table books change column `title` `bookname` varchar(50) not null;
rename table books to BOOKS;
drop table BOOKS;