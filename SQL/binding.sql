drop table if exists binding;
CREATE TABLE binding (
  id INT(11) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) not null,
  PRIMARY KEY (id),
  KEY name(name)
);

INSERT INTO binding VALUES(NULL, "none");
INSERT INTO binding VALUES(NULL, "soft");
INSERT INTO binding VALUES(NULL, "hard");
