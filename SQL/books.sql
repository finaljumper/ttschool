DROP DATABASE IF EXISTS bookstore;
CREATE DATABASE bookstore;
USE bookstore;

CREATE TABLE books (
  id INT(11) NOT NULL AUTO_INCREMENT,
  title VARCHAR(50) NOT NULL,
  year INT(4) NOT NULL,
  pages INT(11) not null,
  PRIMARY KEY (id)
);

INSERT INTO books VALUES(NULL, "book1", 2017, 1);
INSERT INTO books VALUES(NULL, "book2", 2017, 10);
INSERT INTO books VALUES(NULL, "book3", 2017, 12);
INSERT INTO books VALUES(NULL, "bookcow", 2011, 321);
INSERT INTO books VALUES(NULL, "book4", 2017, 8);
INSERT INTO books VALUES(NULL, "futurebook", 2018, 123);
INSERT INTO books VALUES(NULL, "whatbook1", 2014, 345);