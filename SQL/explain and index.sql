explain select * from books limit 3;
explain select * from books where title like "w%";
explain select * from books where year between 2010 and 2015;
explain update books set pages = 52 where pages > 99;
explain delete from books where title like "w%";
explain select * from books where pages between 2 and 10;
explain delete from books where id between 3 and 6;

alter table books add index(title);
alter table books add index(year);
alter table books add index(pages);