use bookstore;
select * from books;
select * from books limit 3;
select * from books where id = 3;
select * from books where title like "w%";
select * from books where title like "%w";
select * from books where title like "book1";
select * from books where year between 2010 and 2015;
select * from books where pages between 2 and 10;
select * from books where year between 2015 and 2017 and pages between 10 and 500;
