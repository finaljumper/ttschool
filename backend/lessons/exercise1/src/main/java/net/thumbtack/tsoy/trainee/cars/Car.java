package net.thumbtack.tsoy.trainee.cars;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import net.thumbtack.tsoy.trainee.interfaces.Colored;
import net.thumbtack.tsoy.trainee.utilities.Color;

public class Car implements Colored{
    private final String mark;
    private final int weight;
    private final int maxSpeed;
    private Color color;

    public Car(String mark, int weight, int maxSpeed, String colorString) throws ColorException {
        this.mark = mark;
        this.weight = weight;
        this.maxSpeed = maxSpeed;
        this.color = Color.colorFromString(colorString);
    }

    public String getMark() {
        return mark;
    }

    public int getWeight() {
        return weight;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void print() {
        System.out.println("Mark: " + mark + ", weight: " + weight + " kg" + ", max speed: " + maxSpeed + " km/h");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;

        Car car = (Car) o;

        if (weight != car.weight) return false;
        if (maxSpeed != car.maxSpeed) return false;
        return mark.equals(car.mark);

    }

    @Override
    public int hashCode() {
        int result = mark.hashCode();
        result = 31 * result + weight;
        result = 31 * result + maxSpeed;
        return result;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void setColor(String colorString) throws ColorException {
        this.color = Color.colorFromString(colorString);
    }

    @Override
    public Color getColor() {
        return color;
    }


}
