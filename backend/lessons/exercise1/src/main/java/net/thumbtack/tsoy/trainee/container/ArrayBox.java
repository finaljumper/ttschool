package net.thumbtack.tsoy.trainee.container;

import net.thumbtack.tsoy.trainee.figures.geometry2D.Rectangle;
import net.thumbtack.tsoy.trainee.figures.geometry3D.Rectangle3D;

public class ArrayBox<T extends Rectangle> {
    private T[] array;

    public ArrayBox(T[] array) {
        this.array = array;
    }

    public static void main(String args[]) {
        Rectangle[] rectangles = new Rectangle[10];
        Rectangle3D[] rectangles3D = new Rectangle3D[10];
        String[] strings = new String[10];
        ArrayBox<Rectangle> rectArray = new ArrayBox<>(rectangles);
        ArrayBox<Rectangle3D> rect3DArray = new ArrayBox<>(rectangles3D);
        //ArrayBox<String> stringArray = new ArrayBox<>(strings);
    }

    public T[] getArray() {
        return array;
    }

    public void setArray(T[] array) {
        this.array = array;
    }

}
