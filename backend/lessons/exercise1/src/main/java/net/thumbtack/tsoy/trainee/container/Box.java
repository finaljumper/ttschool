package net.thumbtack.tsoy.trainee.container;

import net.thumbtack.tsoy.trainee.figures.geometry2D.Rectangle;
import net.thumbtack.tsoy.trainee.figures.geometry3D.Rectangle3D;
import net.thumbtack.tsoy.trainee.interfaces.Square;

public class Box<T extends Rectangle> implements Square {
    private T object;
    private static final double EPS = 1E-6;

    public Box(T object) {
        this.object = object;
    }

    @Override
    public double square() {
        return object.getArea();
    }

    public boolean isSameSquare(Box<Rectangle> box) {
        return Math.abs(this.square() - box.square()) < EPS;
    }

    public static boolean isSameSquareStatic(Box<Rectangle> box1, Box<Rectangle> box2) {
        return Math.abs(box1.square() - box2.square()) < EPS;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }

    public static void main(String args[]) {
        Rectangle rect = new Rectangle();
        Box<Rectangle> box = new Box<>(rect);
        Rectangle3D rect3D = new Rectangle3D();
        Box<Rectangle3D> box1 = new Box<>(rect3D);
        //Box<String> stringBox = new Box<>("string");
    }
}
