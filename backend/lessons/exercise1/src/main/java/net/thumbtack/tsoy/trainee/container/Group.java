package net.thumbtack.tsoy.trainee.container;

import net.thumbtack.tsoy.trainee.exceptions.GroupException;
import net.thumbtack.tsoy.trainee.trainee.Trainee;

public class Group {
    private String name;
    private Trainee[] trainees;

    public Group(String name, Trainee[] trainees) throws GroupException {
        setName(name);
        setTrainees(trainees);
    }

    public void checkName(String name) throws GroupException {
        if (name == null || name.equals(""))
            throw new GroupException("Name is invalid.");
    }

    public void checkTrainees(Trainee[] trainees) throws GroupException {
        if (trainees == null || trainees.length == 0)
            throw new GroupException("Wrong array of trainees.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws GroupException {
        checkName(name);
        this.name = name;
    }

    public Trainee[] getTrainees() {
        return trainees;
    }

    public void setTrainees(Trainee[] trainees) throws GroupException {
        checkTrainees(trainees);
        this.trainees = trainees;
    }

}
