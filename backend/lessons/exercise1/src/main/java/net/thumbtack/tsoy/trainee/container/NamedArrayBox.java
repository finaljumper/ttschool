package net.thumbtack.tsoy.trainee.container;

import net.thumbtack.tsoy.trainee.figures.geometry2D.Rectangle;

/**
 * Created by renai on 09.10.16.
 */

// REVU ArrayBox is a raw type. References to generic type ArrayBox<T> should be parameterized


public class NamedArrayBox extends ArrayBox<Rectangle>{

    private String name;

    public NamedArrayBox(Rectangle[] array, String name) {
        super(array);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
