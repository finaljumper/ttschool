package net.thumbtack.tsoy.trainee.container;

import net.thumbtack.tsoy.trainee.figures.geometry2D.Figure;

public class PairBox<T extends Figure> {
    private final static double EPS = 1E-6;
    private T object1;
    private T object2;

    public PairBox(T object1, T object2) {
        this.object1 = object1;
        this.object2 = object2;
    }

    public boolean isSameSquare() {
        return Math.abs(object1.getArea() - object2.getArea()) < EPS;
    }

    public T getObject1() {
        return object1;
    }

    public void setObject1(T object1) {
        this.object1 = object1;
    }

    public T getObject2() {
        return object2;
    }

    public void setObject2(T object2) {
        this.object2 = object2;
    }
}
