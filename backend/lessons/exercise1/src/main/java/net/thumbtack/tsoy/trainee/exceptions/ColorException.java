package net.thumbtack.tsoy.trainee.exceptions;

final public class ColorException extends Exception{
    private static final long serialVersionUID = -4841776918021553911L;

    /**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public ColorException() {
        super("Color is not valid.");
    }
}
