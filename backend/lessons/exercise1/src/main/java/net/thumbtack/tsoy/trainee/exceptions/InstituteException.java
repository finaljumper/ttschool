package net.thumbtack.tsoy.trainee.exceptions;

public class InstituteException extends Exception {

    private static final long serialVersionUID = -1931242869866275909L;

    /**
     * Constructs a new exception with the specified detail message.  The
     * cause is not initialized, and may subsequently be initialized by
     * a call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public InstituteException(String message) {
        super(message);
    }
}
