package net.thumbtack.tsoy.trainee.exceptions;

import net.thumbtack.tsoy.trainee.trainee.TraineeErrorCodes;

import java.security.PrivilegedActionException;

public class TraineeException extends Exception {

    private static final long serialVersionUID = 4464931951445967486L;

    /**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public TraineeException() {
        super();
    }

    public TraineeException(TraineeErrorCodes errorCode, String parameter) {
        super(String.format(errorCode.getMessage(), parameter));
    }

    public TraineeException(TraineeErrorCodes errorCode) {
        super(errorCode.getMessage());
    }

    public TraineeException(TraineeErrorCodes errorCode, Throwable cause) {
        super(errorCode.getMessage(), cause);
    }

    /**
     * Constructs a new exception with the specified cause and a detail
     * message of <tt>(cause==null ? null : cause.toString())</tt> (which
     * typically contains the class and detail message of <tt>cause</tt>).
     * This constructor is useful for exceptions that are little more than
     * wrappers for other throwables (for example, {@link
     * PrivilegedActionException}).
     *
     * @param cause the cause (which is saved for later retrieval by the
     *              {@link #getCause()} method).  (A <tt>null</tt> value is
     *              permitted, and indicates that the cause is nonexistent or
     *              unknown.)
     * @since 1.4
     */
    public TraineeException(Throwable cause) {
        super(cause);
    }
}
