package net.thumbtack.tsoy.trainee.figures.geometry2D;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;

public class Circle extends Figure {
    private Point2D center;
    private double radius;
    public Circle(double x, double y, double radius, String colorString) throws ColorException {
    	super(colorString);
        center = new Point2D(x, y);
        this.radius = radius;
    }

    public Circle() {
        super();
        center = new Point2D(0, 0);
        radius = 1;
    }

    public void move(double dx, double dy) {
        center.move(dx, dy);
    }

    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    public boolean checkBelonging(double x, double y) {
        return (x - center.getX()) * (x - center.getX())
                + (y - center.getY()) * (y - center.getY()) <= radius * radius;
    }

    public boolean checkBelonging(Point2D temp) {
        double x = temp.getX();
        double y = temp.getY();
        return (x - center.getX()) * (x - center.getX())
                + (y - center.getY()) * (y - center.getY()) <= radius * radius;
    }

    public void printPoints() {
        System.out.print("Center: ");
        center.printCoordinates();
        System.out.println("Radius: " + radius);
    }

    public Point2D getCenter() {
        return center;
    }

    public void setCenter(Point2D center) {
        this.center = center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Circle)) return false;

        Circle circle = (Circle) o;

        if (Double.compare(circle.radius, radius) != 0) return false;
        return center.equals(circle.center);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = center.hashCode();
        temp = Double.doubleToLongBits(radius);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
