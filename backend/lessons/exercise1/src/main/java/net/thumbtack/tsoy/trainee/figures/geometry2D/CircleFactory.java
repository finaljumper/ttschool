package net.thumbtack.tsoy.trainee.figures.geometry2D;

/*

 */
public class CircleFactory {
    private static int quantity = 0;
    public static Circle createCircle() {
        quantity++;
        return new Circle();
    }

    public static int getQuantity() {
        return quantity;
    }
}
