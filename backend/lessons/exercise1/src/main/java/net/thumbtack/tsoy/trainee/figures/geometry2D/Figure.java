package net.thumbtack.tsoy.trainee.figures.geometry2D;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import net.thumbtack.tsoy.trainee.interfaces.Colored;
import net.thumbtack.tsoy.trainee.utilities.Color;


public abstract class Figure implements Colored{
    private Color color;

    protected Figure(String colorString) throws ColorException {
        this.color = Color.colorFromString(colorString);
    }

    protected Figure() {
        color = Color.BLACK;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void setColor(String colorString) throws ColorException {
        this.color = Color.colorFromString(colorString);
    }

    @Override
    public Color getColor() {
        return color;
    }

    abstract public void move(double dx, double dy);

    abstract public boolean checkBelonging(Point2D temp);

    abstract public double getArea();

    abstract public void printPoints();
}
