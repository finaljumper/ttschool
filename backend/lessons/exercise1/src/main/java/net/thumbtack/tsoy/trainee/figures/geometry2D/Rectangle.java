package net.thumbtack.tsoy.trainee.figures.geometry2D;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;

/**
 *
 */
public class Rectangle extends Figure {

    private Point2D lowerLeftPoint;
    private Point2D upperRightPoint;

    /**
     * Set coordinates for lower left point and upper right point of the Rectangle.
     * @param x1 x coordinate of lower left point
     * @param y1 y coordinate of lower left point
     * @param x2 x coordinate of upper right point
     * @param y2 y coordinate of upper right point
     */
    public Rectangle(double x1, double y1, double x2, double y2, String colorString) throws ColorException {
        super(colorString);
        lowerLeftPoint = new Point2D(x1, y1);
        upperRightPoint = new Point2D(x2, y2);
    }

    /**
     * Creates lower left point of rectangle in coordinates (0;0). Set length and width of rectangle.
     * @param length length of the rectangle. Lies on X.
     * @param width width of the rectangle. Lies on Y.
     */
    public Rectangle(double length, double width, String colorString) throws ColorException {
        this(0, 0, length, width, colorString);
    }

    /**
     * Creates a square with size (1;1). Its lower left point lies on (0;0).
     */
    public Rectangle() {
    	super();
        lowerLeftPoint = new Point2D(0, 0);
        upperRightPoint = new Point2D(1, 1);
    }

    public void printPoints() {
        System.out.print("Lower left point:" );
        lowerLeftPoint.printCoordinates();
        System.out.println("Lower right point: x = " + upperRightPoint.getX() + ", y = "
                + lowerLeftPoint.getY());
        System.out.println("Upper left point: x = " + lowerLeftPoint.getX() + ", y = "
                + upperRightPoint.getY());
        System.out.print("Upper right point: ");
        upperRightPoint.printCoordinates();
    }

    public void move(double dx, double dy) {
        lowerLeftPoint.move(dx, dy);
        upperRightPoint.move(dx, dy);
    }

    public boolean checkIntersection(Rectangle temp) {
        boolean xConditionForLowerPoint = lowerLeftPoint.getX() <= temp.lowerLeftPoint.getX()
                && temp.lowerLeftPoint.getX() <= upperRightPoint.getX();
        boolean xConditionForUpperPoint = lowerLeftPoint.getX() <= temp.upperRightPoint.getX()
                && temp.upperRightPoint.getX() <= upperRightPoint.getX();
        boolean yConditionForLowerPoint = lowerLeftPoint.getY() <= temp.lowerLeftPoint.getY()
                && upperRightPoint.getY() >= temp.lowerLeftPoint.getY();
        boolean yConditionForUpperPoint = lowerLeftPoint.getY() <= temp.upperRightPoint.getY()
                && upperRightPoint.getY() >= temp.upperRightPoint.getY();

        return (xConditionForLowerPoint || xConditionForUpperPoint)
                && (yConditionForLowerPoint || yConditionForUpperPoint);
    }

    public void shrink(double nx, double ny) {

        double newX = lowerLeftPoint.getX() + Math.round((upperRightPoint.getX() - lowerLeftPoint.getX()) / nx);
        double newY = lowerLeftPoint.getY() + Math.round((upperRightPoint.getY() - lowerLeftPoint.getY()) / ny);
        upperRightPoint.setY(newY);
        upperRightPoint.setX(newX);
    }

    public double getArea() {
        return (upperRightPoint.getX() - lowerLeftPoint.getX()) * (upperRightPoint.getY() - lowerLeftPoint.getY());
    }

    public boolean checkBelonging(double x, double y) {
        return lowerLeftPoint.getX() <= x && x <= upperRightPoint.getX()
                && lowerLeftPoint.getY() <= y && y <= upperRightPoint.getY();
    }

    public boolean checkBelonging(Point2D temp) {
        double x = temp.getX();
        double y = temp.getY();
        return lowerLeftPoint.getX() <= x && x <= upperRightPoint.getX()
                && lowerLeftPoint.getY() <= y && y <= upperRightPoint.getY();
    }

    public boolean checkEnclosing(Rectangle temp) {
        return lowerLeftPoint.getX() >= temp.getLowerLeftPoint().getX()
                && lowerLeftPoint.getY() >= temp.getLowerLeftPoint().getY()
                && upperRightPoint.getX() <= temp.getUpperRightPoint().getX()
                && upperRightPoint.getY() <= temp.getUpperRightPoint().getY();
    }

    public void large(int multiplier) {
        double newX = lowerLeftPoint.getX() + (upperRightPoint.getX() - lowerLeftPoint.getX()) * multiplier;
        double newY = lowerLeftPoint.getY() + (upperRightPoint.getY() - lowerLeftPoint.getY()) * multiplier;
        upperRightPoint.setY(newY);
        upperRightPoint.setX(newX);
    }

    public Point2D getLowerLeftPoint() {
        return lowerLeftPoint;
    }

    public void setLowerLeftPoint(Point2D lowerLeftPoint) {
        this.lowerLeftPoint = lowerLeftPoint;
    }

    public Point2D getUpperRightPoint() {
        return upperRightPoint;
    }

    public void setUpperRightPoint(Point2D upperRightPoint) {
        this.upperRightPoint = upperRightPoint;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (!lowerLeftPoint.equals(rectangle.lowerLeftPoint)) return false;
        return upperRightPoint.equals(rectangle.upperRightPoint);

    }

    @Override
    public int hashCode() {
        int result = lowerLeftPoint.hashCode();
        result = 31 * result + upperRightPoint.hashCode();
        return result;
    }
}
