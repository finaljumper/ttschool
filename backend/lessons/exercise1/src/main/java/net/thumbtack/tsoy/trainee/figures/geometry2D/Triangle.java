package net.thumbtack.tsoy.trainee.figures.geometry2D;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;

/**
 *
 */

public class Triangle extends Figure {
    final static double EPS = 1E-6;
    private Point2D firstVertex, secondVertex, thirdVertex;

    /**
     * Basic constructor for class Triangle
     * @param x1 x coordinate for 1st point
     * @param y1 y coordinate for 1st point
     * @param x2 x coordinate for 2nd point
     * @param y2 y coordinate for 2nd point
     * @param x3 x coordinate for 3rd point
     * @param y3 y coordinate for 3rd point
     */
    public Triangle(double x1, double y1, double x2, double y2, double x3, double y3, String colorString) throws ColorException {
        super(colorString);
        firstVertex = new Point2D(x1, y1);
        secondVertex = new Point2D(x2, y2);
        thirdVertex = new Point2D(x3, y3);
    }

    public void printPoints() {
        System.out.print("First vertex: ");
        firstVertex.printCoordinates();
        System.out.println();
        System.out.print("Second vertex: ");
        secondVertex.printCoordinates();
        System.out.println();
        System.out.print("Third vertex: ");
        thirdVertex.printCoordinates();
        System.out.println();
    }

    public void move(double dx, double dy) {
        firstVertex.move(dx, dy);
        secondVertex.move(dx, dy);
        thirdVertex.move(dx, dy);
    }

    public double getArea() {
        double area, p, a, b, c;
        a = Math.sqrt((firstVertex.getX() - secondVertex.getX()) * (firstVertex.getX() - secondVertex.getX())
                + (firstVertex.getY() - secondVertex.getY()) * (firstVertex.getY() - secondVertex.getY()));
        b = Math.sqrt((thirdVertex.getX() - secondVertex.getX()) * (thirdVertex.getX() - secondVertex.getX())
                + (thirdVertex.getY() - secondVertex.getY()) * (thirdVertex.getY() - secondVertex.getY()));
        c = Math.sqrt((firstVertex.getX() - thirdVertex.getX()) * (firstVertex.getX() - thirdVertex.getX())
                + (firstVertex.getY() - thirdVertex.getY()) * (firstVertex.getY() - thirdVertex.getY()));
        p = (a + b + c) / 2;
        area = Math.sqrt(p * (p - a) * (p - b) * (p - c));
        return area;
    }

    public boolean checkBelonging(Point2D temp) {
        /*  0 - точка; всё одного знака или есть 0 - true
            (x1 - x0) * (y2 - y1) - (x2 - x1) * (y1 - y0)
            (x2 - x0) * (y3 - y2) - (x3 - x2) * (y2 - y0)
            (x3 - x0) * (y1 - y3) - (x1 - x3) * (y3 - y0)
         */
        boolean result = false;
        double a, b, c;
        a = (firstVertex.getX() - temp.getX()) * (secondVertex.getY() - firstVertex.getY())
                - (secondVertex.getX() - firstVertex.getX()) * (firstVertex.getY() - temp.getY());
        b = (secondVertex.getX() - temp.getX()) * (thirdVertex.getY() - secondVertex.getY())
                - (thirdVertex.getX() - secondVertex.getX()) * (secondVertex.getY() - temp.getY());
        c = (thirdVertex.getX() - temp.getX()) * (firstVertex.getY() - thirdVertex.getY())
                - (firstVertex.getX() - thirdVertex.getX()) * (thirdVertex.getY() - temp.getY());
        if (a == 0 || b == 0 || c == 0)
            result = true;
        if (a > 0 && b > 0 && c > 0)
            result = true;
        if (a < 0 && b < 0 && c < 0)
            result = true;
        return result;
    }

    public boolean checkBelonging(double x, double y) {
        boolean result = false;
        double a, b, c;
        a = (firstVertex.getX() - x) * (secondVertex.getY() - firstVertex.getY())
                - (secondVertex.getX() - firstVertex.getX()) * (firstVertex.getY() - y);
        b = (secondVertex.getX() - x) * (thirdVertex.getY() - secondVertex.getY())
                - (thirdVertex.getX() - secondVertex.getX()) * (secondVertex.getY() - y);
        c = (thirdVertex.getX() - x) * (firstVertex.getY() - thirdVertex.getY())
                - (firstVertex.getX() - thirdVertex.getX()) * (thirdVertex.getY() - y);
        if (Math.abs(a) < EPS || Math.abs(b) < EPS || Math.abs(c) < EPS)
            result = true;
        if (a > 0 && b > 0 && c > 0)
            result = true;
        if (a < 0 && b < 0 && c < 0)
            result = true;
        return result;
    }

    public boolean checkEquilateral() {
        double a, b, c;
        a = Math.sqrt(Math.pow(firstVertex.getX() - secondVertex.getX(), 2)
                + Math.pow(firstVertex.getY() - secondVertex.getY(), 2));
        b = Math.sqrt(Math.pow(thirdVertex.getX() - secondVertex.getX(), 2)
                + Math.pow(thirdVertex.getY() - secondVertex.getY(), 2));
        c = Math.sqrt(Math.pow(firstVertex.getX() - thirdVertex.getX(), 2)
                + Math.pow(firstVertex.getY() - thirdVertex.getY(), 2));
        return Math.abs(a - b) <= EPS && Math.abs(b - c) <= EPS;
    }

    public Point2D getFirstVertex() {
        return firstVertex;
    }

    public void setFirstVertex(Point2D firstVertex) {
        this.firstVertex = firstVertex;
    }

    public Point2D getSecondVertex() {
        return secondVertex;
    }

    public void setSecondVertex(Point2D secondVertex) {
        this.secondVertex = secondVertex;
    }

    public Point2D getThirdVertex() {
        return thirdVertex;
    }

    public void setThirdVertex(Point2D thirdVertex) {
        this.thirdVertex = thirdVertex;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triangle triangle = (Triangle) o;

        if (firstVertex != null ? !firstVertex.equals(triangle.firstVertex) : triangle.firstVertex != null)
            return false;
        if (secondVertex != null ? !secondVertex.equals(triangle.secondVertex) : triangle.secondVertex != null)
            return false;
        return thirdVertex != null ? thirdVertex.equals(triangle.thirdVertex) : triangle.thirdVertex == null;

    }

    @Override
    public int hashCode() {
        int result = firstVertex != null ? firstVertex.hashCode() : 0;
        result = 31 * result + (secondVertex != null ? secondVertex.hashCode() : 0);
        result = 31 * result + (thirdVertex != null ? thirdVertex.hashCode() : 0);
        return result;
    }
}
