package net.thumbtack.tsoy.trainee.figures.geometry3D;


import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import net.thumbtack.tsoy.trainee.figures.geometry2D.Circle;

public class Cylinder extends Circle {

    private double height;

    public Cylinder(double x, double y, double radius, double height, String colorString) throws ColorException {
        super(x, y, radius, colorString);
        this.height = height;
    }

    public Cylinder() {
        super();
        height = 1;
    }

    public double getVolume() {
        return getArea() * height;
    }

    public double getBaseArea() {
        return getArea();
    }

    public double getSideArea() {
        return getPerimeter() * height;
    }

    public boolean checkBelonging(double x, double y, double z) {
        return checkBelonging(x, y) && z <= height;
    }

    public boolean checkBelonging(Point3D temp) {
        return checkBelonging(temp.getX(), temp.getY()) && temp.getZ() <= height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cylinder)) return false;

        Cylinder cylinder = (Cylinder) o;

        return Double.compare(cylinder.height, height) == 0;

    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(height);
        return (int) (temp ^ (temp >>> 32));
    }
}
