package net.thumbtack.tsoy.trainee.figures.geometry3D;


import net.thumbtack.tsoy.trainee.figures.geometry2D.Point2D;

public class Point3D extends Point2D {
    private double z;

    public Point3D(double x, double y, double z) {
        super(x, y);
        this.z = z;
    }

    public Point3D() {
    	this(0, 0, 0);
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public void printCoordinates() {
        System.out.println("x = " + getX() + ", y = " + getY() + ", z = " + getZ());
    }

    public void move(double dx, double dy, double dz) {
        super.move(dx, dy);
        z += dz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Point3D point3D = (Point3D) o;

        return Double.compare(point3D.z, z) == 0;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
