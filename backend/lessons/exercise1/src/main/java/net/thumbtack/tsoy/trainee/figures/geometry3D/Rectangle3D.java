package net.thumbtack.tsoy.trainee.figures.geometry3D;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import net.thumbtack.tsoy.trainee.figures.geometry2D.Figure;
import net.thumbtack.tsoy.trainee.figures.geometry2D.Rectangle;

public class Rectangle3D extends Rectangle {

    private double height;

    /**
     * Set coordinates for lower left point and upper right point of the Rectangle.
     * Rectangle lies on OXY
     *
     * @param x1     x coordinate of lower left base point
     * @param y1     y coordinate of lower left base point
     * @param x2     x coordinate of upper right point
     * @param y2     y coordinate of upper right point
     * @param height height for all points
     */
    public Rectangle3D(double x1, double y1, double x2, double y2, double height, String colorString) throws ColorException {
        super(x1, y1, x2, y2, colorString);
        this.height = height;
    }

    /**
     * Creates lower left point of rectangle in coordinates (0;0). Set length and width of rectangle.
     *
     * @param length length of the rectangle. Lies on X.
     * @param width  width of the rectangle. Lies on Y.
     * @param height height for all points
     */
    public Rectangle3D(double length, double width, double height, String colorString) throws ColorException {
        super(length, width, colorString);
        this.height = height;
    }

    /**
     * Creates a cube with size (1;1;1). Its lower left base point lies on (0;0;0).
     */
    public Rectangle3D() {
        super();
        height = 1;
    }

    @Override
    public void printPoints() {
        super.printPoints();
        System.out.println("Height = " + height);
    }

/*
    public double getVolume() {
        return super.getArea() * (upperRightPoint.getZ() - lowerLeftBasePoint.getZ());
    }
*/

    public boolean checkBelonging(Point3D temp) {
        return super.checkBelonging(temp.getX(), temp.getY())
                && (0 <= temp.getZ() && height >= temp.getZ());
    }

    public boolean checkBelonging(double x, double y, double z) {
        return super.checkBelonging(x, y)
                && (0 <= z && height >= z);
    }

    public boolean checkEnclosing(Rectangle3D temp) {
        return super.checkEnclosing(temp) && temp.getHeight() >= height;
    }

    public boolean checkIntersection(Rectangle3D temp) {
        return super.checkIntersection(temp);
    }

    public static void main(String args[]) {
        double volume;
        Rectangle test = new Rectangle3D();
        test.printPoints();
        volume = new Rectangle3D() {
            double getVolume() {
                return getArea() * getHeight();
            }
        }.getVolume();
        Figure temp = new Rectangle();
        temp.printPoints();
        temp = new Rectangle3D();
        temp.printPoints();
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rectangle3D)) return false;
        if (!super.equals(o)) return false;

        Rectangle3D that = (Rectangle3D) o;

        return Double.compare(that.height, height) == 0;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(height);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}

