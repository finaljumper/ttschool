package net.thumbtack.tsoy.trainee.interfaces;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import net.thumbtack.tsoy.trainee.utilities.Color;

public interface Colored {

    void setColor(Color color);
    void setColor(String colorString) throws ColorException;
    Color getColor();
}
