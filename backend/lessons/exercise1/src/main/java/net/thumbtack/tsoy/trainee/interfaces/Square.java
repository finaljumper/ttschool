package net.thumbtack.tsoy.trainee.interfaces;

/**
 * Created by renai on 02.10.16.
 */
public interface Square {
    double square();
}
