package net.thumbtack.tsoy.trainee.trainee;

import net.thumbtack.tsoy.trainee.exceptions.TraineeException;

import java.io.Serializable;

//
public class Trainee implements Serializable, Comparable<Trainee> {
    private static final long serialVersionUID = 9084540009329281028L;
    private String firstName;
    private String lastName;
    private int mark;

    public Trainee(String firstName, String lastName, int mark) throws TraineeException {
        setFirstName(firstName);
        setLastName(lastName);
        setMark(mark);
    }

    private void checkFirstName(String name) throws TraineeException {
        if (name == null || name.equals(""))
            throw new TraineeException(TraineeErrorCodes.WRONG_FIRSTNAME);
    }

    private void checkLastName(String name) throws TraineeException {
        if (name == null || name.equals(""))
            throw new TraineeException(TraineeErrorCodes.WRONG_LASTNAME);
    }

    private void checkMark(int mark) throws TraineeException {
        if (mark < 1 || mark > 5)
            throw new TraineeException(TraineeErrorCodes.WRONG_MARK);
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getMark() {
        return mark;
    }

    public void setFirstName(String firstName) throws TraineeException {
        checkFirstName(firstName);
        this.firstName = firstName;
    }

    public void setLastName(String lastName) throws TraineeException {
        checkLastName(lastName);
        this.lastName = lastName;
    }

    public void setMark(int mark) throws TraineeException {
        checkMark(mark);
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trainee)) return false;

        Trainee trainee = (Trainee) o;

        if (mark != trainee.mark) return false;
        if (!firstName.equals(trainee.firstName)) return false;
        return lastName.equals(trainee.lastName);

    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + mark;
        return result;
    }

    @Override
    public int compareTo(Trainee o) {
        return this.getFirstName().compareTo(o.getFirstName());
    }
}
