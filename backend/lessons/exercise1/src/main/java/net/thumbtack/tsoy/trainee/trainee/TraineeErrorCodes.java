package net.thumbtack.tsoy.trainee.trainee;

public enum TraineeErrorCodes{
    WRONG_FIRSTNAME("First name is not valid"),
    WRONG_LASTNAME("Last name is not valid"),
    WRONG_MARK("Mark must be from 1 to 5");

    private String message;

    private TraineeErrorCodes(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
