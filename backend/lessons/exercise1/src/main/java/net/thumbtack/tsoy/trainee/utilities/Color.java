package net.thumbtack.tsoy.trainee.utilities;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;

public enum Color{
    RED, GREEN, BLUE, YELLOW, WHITE, BLACK;

    public static Color colorFromString(String color) throws ColorException {
        try{
            return Color.valueOf(color);
        } catch (IllegalArgumentException|NullPointerException e) {
            throw new ColorException();
        }

    }
}