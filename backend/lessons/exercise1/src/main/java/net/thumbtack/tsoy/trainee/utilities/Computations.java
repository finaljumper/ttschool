package net.thumbtack.tsoy.trainee.utilities;

public class Computations {
    public static void numbersInfo(int a, int b) {
        System.out.println(a + "+" + b + "=" + sum(a, b));
        System.out.println(a + "*" + b + "=" + composition(a, b));
        System.out.println(a + "/" + b + "=" + division(a, b));
        System.out.println(a + " mod " + b + "=" + modulo(a, b));
        if (a > b) {
            System.out.println(a + ">" + b);
        } else if (a < b) {
            System.out.println(a + "<" + b);
        } else {
            System.out.println(a + "=" + b);
        }
    }
    public static int sum(int a, int b) {
        return a + b;
    }
    public static int composition(int a, int b) {
        return a * b;
    }
    public static int division(int a, int b) {
        return a / b;
    }
    public static int modulo(int a, int b) {
        return a % b;
    }
    public static void numbersInfo(double a, double b) {
        System.out.println(a + "+" + b + "=" + sum(a, b));
        System.out.println(a + "*" + b + "=" + composition(a, b));
        System.out.println(a + "/" + b + "=" + division(a, b));
        System.out.println(a + " mod " + b + "=" + modulo(a, b));
        if (a > b) {
            System.out.println(a + ">" + b);
        } else if (a < b) {
            System.out.println(a + "<" + b);
        } else {
            System.out.println(a + "=" + b);
        }
    }
    public static double sum(double a, double b) {
        return a + b;
    }
    public static double composition(double a, double b) {
        return a * b;
    }
    public static double division(double a, double b) {
        return a / b;
    }
    public static double modulo(double a, double b) {
        return a % b;
    }
    public static boolean checkPoint(double x1, double y1, double x2, double y2, double pointX, double pointY) {
        boolean belonging = false;
        double left, down, right, up;
        if (x1 < x2) {
            left = x1;
            right = x2;
        } else {
            left = x2;
            right = x1;
        }
        if (y1 < y2) {
            down = y1;
            up = y2;
        } else {
            down = y2;
            up = y1;
        }
        if (left <= pointX && pointX <= right && down <= pointY && pointY <= up) {
            belonging = true;
        }
        return belonging;
    }
    public static int arraySum(int array[]) {
        int result = 0;
        for (int element: array) {
            result += element;
        }
        return result;
    }
    public static int arrayComposition(int array[]) {
        int result = 1;
        for (int element: array) {
            result *= element;
        }
        return result;
    }
    public static int arrayMax(int array[]) {
        int max = array[0];
        for (int element: array) {
            if (max < element) {
                max = element;
            }
        }
        return max;
    }
    public static int arrayMin(int array[]) {
        int min = array[0];
        for (int element: array) {
            if (min > element) {
                min = element;
            }
        }
        return min;
    }
    public static double arrayMid(int array[]) {
        int mid;
        mid = arraySum(array)/array.length;
        return mid;
    }
    /**
     * Check for increasing order of elements in array
     * @param array contains integer numbers
     * @return {@code true} if elements are sorted in increasing order
     */
    public static boolean isIncreasing(int array[]) {
        int temp = array[0];
        for (int element: array) {
            if (temp > element) {
                return false;
            }
            temp = element;
        }
        return true;
    }

    /**
     * Check for decreasing order of elements in array
     * @param array contains integer numbers
     * @return {@code true} if elements are sorted in decreasing order
     */
    public static boolean isDecreasing(int array[]) {
        int temp = array[0];
        for (int element: array) {
            if (temp < element) {
                return false;
            }
            temp = element;
        }
        return true;
    }
}
