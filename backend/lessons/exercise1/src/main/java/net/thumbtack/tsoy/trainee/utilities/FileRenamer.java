package net.thumbtack.tsoy.trainee.utilities;

import java.io.File;
import java.io.FilenameFilter;

public final class FileRenamer {
    private static FilenameFilter filter = (dir, name) -> name.length() > 4 && name.endsWith(".dat");
    public static void rename(String directory) {
        File dir = new File(directory);
        File[] files = dir.listFiles(filter);
        StringBuilder temp;
        assert files != null;
        for (File file:files) {
            temp = new StringBuilder(file.getName());
            temp.replace(temp.length() - 4, temp.length(), ".bin");
            temp.insert(0, file.getParentFile() + "/");
            file.renameTo(new File(temp.toString()));
        }
    }

    public static void main(String args[]) {
        FileRenamer.rename("/home/renai/test2/");
    }
}
