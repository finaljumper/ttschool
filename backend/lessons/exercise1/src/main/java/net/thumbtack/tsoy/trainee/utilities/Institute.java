package net.thumbtack.tsoy.trainee.utilities;


import net.thumbtack.tsoy.trainee.exceptions.InstituteException;

public class Institute {
    private String name;
    private String city;

    public Institute(String name, String city) throws InstituteException {
        setName(name);
        setName(city);
    }

    public void checkName(String name) throws InstituteException {
        if (name == null || name.equals(""))
            throw new InstituteException("Name is invalid.");
    }

    public void checkCity(String city) throws InstituteException {
        if (city == null || name.equals(""))
            throw new InstituteException("City is invalid.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws InstituteException {
        checkName(name);
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) throws InstituteException {
        checkCity(city);
        this.city = city;
    }
}
