package net.thumbtack.tsoy.trainee.utilities;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Created by renai on 19.09.16.
 */
public class TypeActions {
    public String message = "Message for test";
    public String unformattedMessage = "message for test";
    public String simpleString = "simple";
    public StringBuilder builder = new StringBuilder();
    public static void main ( String[] args ) {
        Integer val1 = 10;
        Double val2 = 0.01;
        int val3 = val1;
        double val4 = val2;
        BigInteger bigInteger = new BigInteger("9987654321");
        BigDecimal bigDecimal = new BigDecimal("0.005E-23");
        BigDecimal sum = bigDecimal.add(new BigDecimal(bigInteger));
        System.out.println(val3);
        System.out.println(val4);
        System.out.println(sum.toString());
    }
}
