package net.thumbtack.tsoy.trainee.container;


import net.thumbtack.tsoy.trainee.exceptions.TraineeException;
import net.thumbtack.tsoy.trainee.trainee.Trainee;
import net.thumbtack.tsoy.trainee.utilities.Color;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class CollectionTest {

    @Test
    public void listTest() {
        //ArrayList<trainee> trainees = new ArrayList<>(5);
        List<Trainee> trainees = new LinkedList<>();
        try {
            trainees.add(new Trainee("Eric", "Cartman", 2));
            trainees.add(new Trainee("Derrick", "Tonn", 4));
            trainees.add(new Trainee("Zick", "Zipper", 1));
            trainees.add(new Trainee("Coca", "Cola", 3));
        } catch (TraineeException e) {
            fail();
        }
        Collections.reverse(trainees);
        assertEquals(trainees.get(3).getFirstName(), "Eric");
        Collections.rotate(trainees, 2);
        assertEquals(trainees.get(1).getFirstName(), "Eric");
        Collections.shuffle(trainees);
        Trainee maxMarkTrainee = Collections.max(trainees, (o1, o2) -> {
            if (o1.getMark() > o2.getMark())
                return 1;
            else if (o1.getMark() < o2.getMark())
                return -1;
            else
                return 0;
        });
        assertEquals(maxMarkTrainee.getMark(), 4);
        Collections.sort(trainees, (o1, o2) -> {
            if (o1.getMark() > o2.getMark())
                return 1;
            if (o1.getMark() < o2.getMark())
                return -1;
            else
                return 0;
        });
        assertEquals(trainees.get(3).getMark(), 4);
        Collections.sort(trainees);
        assertEquals(trainees.get(0).getFirstName(), "Coca");
        Trainee toFind;
        String mockName = "mock";
        int mockMark = 5;
        try {
            toFind = trainees.get(Collections.binarySearch(trainees, new Trainee("Eric", mockName, mockMark)));
            assertEquals(toFind.getFirstName(), "Eric");
        } catch (TraineeException e) {
            fail();
        }
    }

    @Test
    public void arrayListVsLinkedList() {
        Random random = new Random(System.currentTimeMillis());
        long timeStart, timeEnd;
        List<Integer> arrayList = new ArrayList<>(100000);
        List<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < 100000; i++) {
            arrayList.add(i);
            linkedList.add(i);
        }
        timeStart = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            arrayList.get(random.nextInt(100000));
        }
        timeEnd = System.nanoTime();
        System.out.println("ArrayList time: " + TimeUnit.NANOSECONDS.toMillis(timeEnd - timeStart) + " ms");
        timeStart = System.nanoTime();
        for (int i = 0; i < 100000; i++) {
            linkedList.get(random.nextInt(100000));
        }
        timeEnd = System.nanoTime();
        System.out.println("LinkedList time: " + TimeUnit.NANOSECONDS.toMillis(timeEnd - timeStart) + " ms");
    }

    @Test
    public void queueTest() {
        Trainee one = null, two = null;
        try {
            one = new Trainee("Dilan", "Rogers", 5);
            two = new Trainee("Gleb", "Lipatnikov", 2);
        } catch (TraineeException e) {
            fail();
        }
        Queue<Trainee> queue = new LinkedList<>();
        queue.add(one);
        queue.add(two);
        queue.add(two);
        for (int i = 0; i < 3; i++) {
            queue.poll();
        }
        assertEquals(queue.isEmpty(), true);
    }

    @Test
    public void hashSetTest() {
        HashSet<Trainee> hashSet = new HashSet<>();
        Trainee one = null, two = null, three = null;
        try {
            one = new Trainee("Dilan", "Rogers", 5);
            two = new Trainee("Gleb", "Lipatnikov", 2);
            three = new Trainee("Gleb", "Ivanov", 3);
        } catch (TraineeException e) {
            fail();
        }
        hashSet.add(one);
        hashSet.add(two);
        assertEquals(hashSet.contains(three), false);
        for(Trainee i : hashSet)
            System.out.println(i);
    }

    @Test
    public void setTest() {
        TreeSet<Trainee> treeSet = new TreeSet<>();
        Trainee one = null, two = null, three = null, four = null;
        try {
            one = new Trainee("Dilan", "Rogers", 5);
            two = new Trainee("Gleb", "Lipatnikov", 2);
            three = new Trainee("Gleb", "Ivanov", 3);
            four = new Trainee("Henry", "Brian", 4);
        } catch (TraineeException e) {
            fail();
        }
        treeSet.add(one);
        treeSet.add(two);
        //should change compareTo() method for valid checking
        assertEquals(treeSet.contains(three), true);
        for(Trainee i : treeSet)
            System.out.println(i);
        SortedSet<Trainee> sortedSet = new TreeSet<>();
        sortedSet.add(one);
        sortedSet.add(two);
        sortedSet.add(four);
        assertEquals(sortedSet.contains(three), true);
        for(Trainee i : sortedSet)
            System.out.println(i);
    }

    @Test
    public void hashSetVsListVsTreeSet() {
        Random random = new Random(System.currentTimeMillis());
        long timeStart, timeEnd;
        List<Integer> arrayList = new ArrayList<>(100000);
        Set<Integer> hashSet = new HashSet<>(100000);
        Set<Integer> treeSet = new TreeSet<>();
        for (int i = 0; i < 100000; i++) {
            arrayList.add(random.nextInt());
        }
        while (hashSet.size() < 100000)
            hashSet.add(random.nextInt());
        while (treeSet.size() < 100000)
            treeSet.add(random.nextInt());
        timeStart = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            arrayList.contains(random.nextInt());
        }
        timeEnd = System.nanoTime();
        System.out.println("ArrayList time: " + TimeUnit.NANOSECONDS.toMillis(timeEnd - timeStart) + " ms");
        timeStart = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            treeSet.contains(random.nextInt());
        }
        timeEnd = System.nanoTime();
        System.out.println("TreeSet time: " + TimeUnit.NANOSECONDS.toMillis(timeEnd - timeStart) + " ms");
        timeStart = System.nanoTime();
        for (int i = 0; i < 10000; i++) {
            hashSet.contains(random.nextInt());
        }
        timeEnd = System.nanoTime();
        System.out.println("HashSet time: " + TimeUnit.NANOSECONDS.toMillis(timeEnd - timeStart) + " ms");
    }

    @Test
    public void bitSetTest() {
        BitSet bitSet = new BitSet();
        bitSet.set(5, 25);
        assertEquals(bitSet.get(10), true);
        bitSet.clear(10);
        assertEquals(bitSet.get(10), false);
    }

    @Test
    public void setSpeedTest() {
        long timeStart, timeEnd;
        BitSet bitSet = new BitSet();
        Set<Integer> hashSet = new HashSet<>(1000000);
        Set<Integer> treeSet = new TreeSet<>();
        timeStart = System.nanoTime();
        bitSet.set(0, 1000000);
        timeEnd = System.nanoTime();
        System.out.println("BitSet time: " + TimeUnit.NANOSECONDS.toMillis(timeEnd - timeStart) + " ms");
        timeStart = System.nanoTime();
        for (int i = 0; i < 1000000; i++) {
            hashSet.add(i);
        }
        timeEnd = System.nanoTime();
        System.out.println("HashSet time: " + TimeUnit.NANOSECONDS.toMillis(timeEnd - timeStart) + " ms");
        timeStart = System.nanoTime();
        for (int i = 0; i < 1000000; i++) {
            treeSet.add(i);
        }
        timeEnd = System.nanoTime();
        System.out.println("TreeSet time: " + TimeUnit.NANOSECONDS.toMillis(timeEnd - timeStart) + " ms");
    }

    @Test
    public void enumSetTest() {
        EnumSet<Color> all = EnumSet.allOf(Color.class);
        assertEquals(all.contains(Color.BLACK), true);
        EnumSet<Color> none = EnumSet.noneOf(Color.class);
        assertEquals(none.contains(Color.BLACK), false);
        EnumSet<Color> yellow = EnumSet.of(Color.YELLOW);
        assertEquals(yellow.contains(Color.BLACK), false);
        EnumSet<Color> fromGreenToWhite = EnumSet.range(Color.GREEN, Color.WHITE);
        assertEquals(fromGreenToWhite.contains(Color.BLACK), false);
    }

    @Test
    public void uniqueLinesSet() {
        Set<Set<Integer>> setOfSets = new HashSet<>();
        BitSet lineNumbers = new BitSet();
        Set<Integer> line;
        Integer[][] matrix = new Integer[3][3];
        int sizeOfSets = 0;
        for (int i = 0; i < 3; i++) {
            matrix[0][i] = i;
            matrix[1][i] = 2 - i;
            matrix[2][i] = i + 5;
        }
        for (Integer[] nums : matrix) {
            for (Integer num : nums) {
                System.out.print(num + " ");
            }
            System.out.println();
        }
        for (int i = 0; i < 3; i++) {
            line = new HashSet<>();
            Collections.addAll(line, matrix[i]);
            setOfSets.add(line);
            if (sizeOfSets < setOfSets.size()) {
                sizeOfSets++;
                lineNumbers.set(i);
            }
        }
        assertEquals(lineNumbers.get(0), true);
        assertEquals(lineNumbers.get(1), false);
        assertEquals(lineNumbers.get(2), true);
    }
}
