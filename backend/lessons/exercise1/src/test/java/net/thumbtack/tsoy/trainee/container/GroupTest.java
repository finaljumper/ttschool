package net.thumbtack.tsoy.trainee.container;

import net.thumbtack.tsoy.trainee.exceptions.GroupException;
import net.thumbtack.tsoy.trainee.exceptions.TraineeException;
import net.thumbtack.tsoy.trainee.trainee.Trainee;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;


public class GroupTest {

    @Test
    public void test() {
        Group group1 = null;
        Group group2 = null;
        Trainee[] trainees = new Trainee[3];
        try {
            trainees[0] = new Trainee("Eric", "Cartman", 2);
            trainees[1] = new Trainee("Derrick", "Tonn", 4);
            trainees[2] = new Trainee("Zick", "Zipper", 1);
        } catch (TraineeException e) {
            fail();
        }
        try {
            group1 = new Group("group1", trainees);
            group2 = new Group("", trainees);
        } catch (GroupException e) {
            assertEquals(group2, null);
        }
        assertEquals(group2, null);
        assertEquals(group1.getName(), "group1");
        Trainee[] sorted;
        sorted = group1.getTrainees();
        Arrays.sort(sorted, (o1, o2) -> {
            if (o1.getMark() > o2.getMark())
                return 1;
            else if (o1.getMark() < o2.getMark())
                return -1;
            else
                return 0;
        });
        assertEquals(sorted[0].getMark(), 1);
        Arrays.sort(sorted);
        assertEquals(sorted[0].getFirstName(), "Derrick");
        String name = "Zick";
        String mockName = "mock";
        Trainee toFind;
        int mockMark = 5;
        try {
            toFind = sorted[Arrays.binarySearch(sorted, new Trainee(name, mockName, mockMark))];
            assertEquals(toFind.getFirstName(), "Zick");
        } catch (TraineeException e) {
            fail();
        }
    }
}