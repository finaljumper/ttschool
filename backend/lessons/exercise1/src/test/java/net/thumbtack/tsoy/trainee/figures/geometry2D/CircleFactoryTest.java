package net.thumbtack.tsoy.trainee.figures.geometry2D;

import org.junit.Test;

import static org.junit.Assert.*;

public class CircleFactoryTest {
    final static double EPS = 1E-6;
    CircleFactory factory = new CircleFactory();
    @Test
    public void test() {
        Circle test = factory.createCircle();
        assertEquals(factory.getQuantity(), 1);
    }
}