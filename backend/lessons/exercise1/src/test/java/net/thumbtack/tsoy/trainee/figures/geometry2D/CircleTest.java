package net.thumbtack.tsoy.trainee.figures.geometry2D;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import org.junit.Test;

import static org.junit.Assert.*;

public class CircleTest {
    final static double EPS = 1E-6;
    @Test
    public void test() {
        Circle circle = null;
        try {
            circle = new Circle(5, 5, 6, "BLACK");
        } catch (ColorException e) {
            fail();
        }
        assertEquals(circle.checkBelonging(1, 1), true);
        assertEquals(circle.getArea(), 36 * Math.PI, EPS);
        assertEquals(circle.getPerimeter(), 12 * Math.PI, EPS);
    }
}