package net.thumbtack.tsoy.trainee.figures.geometry2D;

import org.junit.Test;

import static org.junit.Assert.*;


public class Point2DTest {
    Point2D testObject = new Point2D();
    Point2D testObject1 = new Point2D(0, 0);
    final static double EPS = 1E-6;
    @Test
    public void test() {
        assertEquals(testObject.equals(testObject1), true);
        testObject.setX(8);
        assertEquals(testObject.getX(), 8, EPS);
        testObject.setY(6);
        assertEquals(testObject.getY(), 6, EPS);
        testObject1.move(8, 6);
        assertEquals(testObject.hashCode(), testObject1.hashCode());
    }
}