package net.thumbtack.tsoy.trainee.figures.geometry2D;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 */
public class RectangleTest {
    final static double EPS = 1E-6;
    Rectangle r1 = new Rectangle();
    Rectangle r2 = new Rectangle();
    @Test
    public void test() {
        Point2D zero = new Point2D();
        assertEquals(r1.getLowerLeftPoint(), zero);
        r1.move(1, 0);
        assertEquals(r1.getUpperRightPoint(), new Point2D(2, 1));
        assertEquals(r1.checkEnclosing(r2), false);
        assertEquals(r1.checkIntersection(r2), true);
        assertEquals(r1.checkBelonging(new Point2D(2, 2)), false);
        r1.large(2);
        assertEquals(r1.getUpperRightPoint().getY(), 2, EPS);
        r1.shrink(1, 1);
        assertEquals(r1.getLowerLeftPoint().getX(), 1, EPS);
        assertEquals(r1.getArea(), 4, EPS);
    }
}