package net.thumbtack.tsoy.trainee.figures.geometry2D;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TriangleTest {

    final static double EPS = 1E-6;
    @Test
    public void test() {
        Triangle test = null;
        try {
            test = new Triangle(1, 1, 1, 3, 3, 1, "BLACK");
        } catch (ColorException e) {
            fail();
        }
        Triangle equal = null;
        try {
            equal = new Triangle(0, 0, 0.5, Math.sqrt(3) / 2, 1, 0, "BLACK");
        } catch (ColorException e) {
            fail();
        }
        test.printPoints();
        test.move(1, 1);
        assertEquals(test.getFirstVertex().getX(), 2, EPS);
        assertEquals(test.getArea(), 2, EPS);
        assertEquals(test.checkBelonging(new Point2D(2, 3)), true);
        assertEquals(equal.checkEquilateral(), true);
    }


}