package net.thumbtack.tsoy.trainee.figures.geometry3D;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CylinderTest {
    final static double EPS = 1E-6;
    @Test
    public void test() {
        Cylinder temp = new Cylinder();
        assertEquals(temp.getSideArea(), 2 * Math.PI, EPS);
        assertEquals(temp.checkBelonging(0.5, 0.7, 0), true);
    }
}