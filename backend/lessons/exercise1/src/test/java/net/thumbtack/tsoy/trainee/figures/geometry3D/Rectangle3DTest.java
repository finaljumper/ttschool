package net.thumbtack.tsoy.trainee.figures.geometry3D;

import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import org.junit.Test;

import static org.junit.Assert.*;

public class Rectangle3DTest {
    final static double EPS = 1E-6;

    @Test
    public void volumeMethod() {
        assertEquals(new Rectangle3D() {
            double getVolume() {
                return getArea() * getHeight();
            }
        }.getVolume() , 1, EPS);
    }
    @Test
    public void test() {
        try {
            Rectangle3D small = new Rectangle3D(0, 0, 4, 4, 4, "BLACK");
            Rectangle3D big = new Rectangle3D(6, 8, 10, "WHITE");
            assertEquals(small.checkEnclosing(big), true);
            assertEquals(small.checkIntersection(big), true);
            Point3D highForSmall = new Point3D(3, 4, 5);
            assertEquals(small.checkBelonging(highForSmall), false);
            assertEquals(big.checkBelonging(highForSmall), true);
        } catch (ColorException e) {
            fail();
        }
    }
}