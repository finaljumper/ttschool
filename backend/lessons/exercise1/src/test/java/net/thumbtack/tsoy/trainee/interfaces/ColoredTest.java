package net.thumbtack.tsoy.trainee.interfaces;

import net.thumbtack.tsoy.trainee.cars.Car;
import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import net.thumbtack.tsoy.trainee.figures.geometry3D.Rectangle3D;
import net.thumbtack.tsoy.trainee.utilities.Color;
import org.junit.Test;

import static org.junit.Assert.*;


public class ColoredTest {

    @Test
    public void assignment() {
        Colored rect = new Rectangle3D();
        rect.setColor(Color.BLUE);
        assertEquals(rect.getColor(), Color.BLUE);
        Colored car = null;
        try {
            car = new Car("Toyota", 1500, 220, "GREEN");
        } catch (ColorException e) {
            fail();
        }
        assertEquals(car.getColor(), Color.GREEN);
    }
}