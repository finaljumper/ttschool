package net.thumbtack.tsoy.trainee.trainee;

import net.thumbtack.tsoy.trainee.exceptions.TraineeException;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TraineeTest {
    @Test
    public void test(){
        Trainee person1 = null, person2 = null;
        try {
            person1 = new Trainee("Michael", "Fench", 4);
            person2 = new Trainee("", "Fench", 2);
            person1.setFirstName("");
        } catch (TraineeException e) {
        	assertEquals(person2, null);
        }
        assertEquals(person1.getFirstName(), "Michael");
    }
}