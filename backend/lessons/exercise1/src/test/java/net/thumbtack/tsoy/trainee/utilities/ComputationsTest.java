package net.thumbtack.tsoy.trainee.utilities;

import org.junit.Test;

import static org.junit.Assert.*;


public class ComputationsTest {
    final static double EPS = 1E-6;
    @Test
    public void testOperations() {
        assertEquals(Computations.sum(2, 3), 5);
        assertEquals(Computations.composition(2, 3), 6);
        assertEquals(Computations.division(5, 2), 2);
        assertEquals(Computations.modulo(5, 2), 1);
        assertEquals(Computations.sum(0.8, 0.2), 1.0, EPS);
        assertEquals(Computations.composition(0.5, 0.5), 0.25, EPS);
        assertEquals(Computations.division(0.2, 0.2), 1.0, EPS);
        assertEquals(Computations.modulo(1.7, 2.5), 1.7, EPS);
    }

    @Test
    public void testPointCheck() {
        assertEquals(Computations.checkPoint(1., 1., 4., 3., 2., 2.), true);
    }

    @Test
    public void arrayTests() {
        int array[] = {1, 2, 3, 4, 5};
        assertEquals(Computations.arraySum(array), 15);
        assertEquals(Computations.arrayComposition(array), 120);
        assertEquals(Computations.arrayMin(array), 1);
        assertEquals(Computations.arrayMax(array), 5);
        assertEquals(Computations.arrayMid(array), 3., EPS);
        assertEquals(Computations.isDecreasing(array), false);
        assertEquals(Computations.isIncreasing(array), true);
    }
}