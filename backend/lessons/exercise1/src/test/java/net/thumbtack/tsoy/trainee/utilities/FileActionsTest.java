package net.thumbtack.tsoy.trainee.utilities;

import com.google.gson.Gson;
import net.thumbtack.tsoy.trainee.exceptions.ColorException;
import net.thumbtack.tsoy.trainee.exceptions.TraineeException;
import net.thumbtack.tsoy.trainee.figures.geometry2D.Rectangle;
import net.thumbtack.tsoy.trainee.trainee.Trainee;
import org.junit.Test;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

public class FileActionsTest {
    @Test
    public void testFileActions() {
        final String home = System.getProperty("user.home");
        final File temp = new File(home + "/test.txt");
        File file = new File("test.txt");
        final File temp2 = new File(home + "/test");
        assertEquals(file.exists(), false);
        File path = new File(home);
        path.mkdir();
        assertEquals(path.isDirectory(), true);
        try {
            file.createNewFile();
        } catch (IOException e) {
            fail();
        }
        assertEquals(file.exists(), true);
        path = new File(home + "/test.txt");
        file.renameTo(path);
        file.delete();
        assertEquals(path.exists(), true);
        assertEquals(path.isFile(), true);
        assertEquals(file.exists(), false);
        assertEquals(path.getAbsolutePath(), temp.getAbsolutePath());
        path.delete();
        assertEquals(path.exists(), false);
        path = new File(home + "/test");
        path.mkdir();
        file = new File(home + "/test/test2.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            fail();
        }
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return dir.getPath().equals(temp2.getAbsolutePath());
            }
        };
        String[] list = path.list(filter);
        if (list != null)
            assertEquals(list[0], "test2.txt");
        else
            fail("List with filename expected");
        file.delete();
    }

    @Test
    public void rectangleTest() {
        Rectangle[] rect = new Rectangle[5];
        Rectangle[] read = new Rectangle[5];
        StringBuilder sb = new StringBuilder();
        try {
            rect[0] = new Rectangle(10., 8., "RED");
            rect[1] = new Rectangle(5., 4., "GREEN");
            rect[2] = new Rectangle(6., 6., "RED");
            rect[3] = new Rectangle(2., 5., "YELLOW");
            rect[4] = new Rectangle();
        } catch (ColorException e) {
            fail();
        }
        for (int i = 0; i < 5; i++) {
            sb.append(rect[i].getLowerLeftPoint().getX()).append(" ");
            sb.append(rect[i].getLowerLeftPoint().getY()).append(" ");
            sb.append(rect[i].getUpperRightPoint().getX()).append(" ");
            sb.append(rect[i].getUpperRightPoint().getY()).append(" ");
            sb.append(rect[i].getColor()).append(" ");
        }
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("rect.txt"))) {
            bos.write(sb.toString().getBytes());
        } catch (IOException e) {
            fail();
        }
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream("rect.txt"))) {
            byte[] array = new byte[400];
            String[] words;
            bis.read(array);
            String temp = new String(array);
            words = temp.split(" ");
            int i = words.length - 2;
            do {
                int index = i / 5;
                double x1 = Double.parseDouble(words[i - 4]);
                double y1 = Double.parseDouble(words[i - 3]);
                double x2 = Double.parseDouble(words[i - 2]);
                double y2 = Double.parseDouble(words[i - 1]);
                String color = words[i];
                try {
                    read[index] = new Rectangle(x1, y1, x2, y2, color);
                } catch (ColorException e) {
                    fail();
                }
                i = i - 5;
            } while (i >= 0);
        } catch (IOException e) {
            fail();
        }
        assertEquals(read[4], rect[4]);
        assertEquals(rect[0], read[0]);
        try (PrintStream ps = new PrintStream(new File("1.dat"))) {
            ps.print(rect[0].getLowerLeftPoint().getX() + " ");
            ps.print(rect[0].getLowerLeftPoint().getY() + " ");
            ps.print(rect[0].getUpperRightPoint().getX() + " ");
            ps.print(rect[0].getUpperRightPoint().getY() + " ");
            ps.print(rect[0].getColor() + " ");
        } catch (FileNotFoundException e) {
            fail();
        }
        File toDelete = new File("rect.txt");
        File toDelete2 = new File("1.dat");
        toDelete.delete();
        toDelete2.delete();
    }

    @Test
    public void traineeTest() {
        Trainee trainee = null;
        Trainee fromFile = null;
        File test = new File("test.txt");
        String temp = null;
        try {
            trainee = new Trainee("Den", "Keen", 4);
        } catch (TraineeException e) {
            fail();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(trainee.getFirstName()).append("\n");
        sb.append(trainee.getLastName()).append("\n");
        sb.append(trainee.getMark()).append("\n");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(test))) {
            bw.write(sb.toString());
        } catch (IOException e) {
            fail();
        }
        try (BufferedReader br = new BufferedReader(new FileReader(test))) {
            try {
                fromFile = new Trainee(br.readLine(), br.readLine(), Integer.parseInt(br.readLine()));
            } catch (NumberFormatException | TraineeException e) {
                fail();
            }
        } catch (IOException e) {
            fail();
        }
        test.delete();
        assertEquals(trainee, fromFile);
        sb = new StringBuilder();
        sb.append(trainee.getFirstName()).append(" ");
        sb.append(trainee.getLastName()).append(" ");
        sb.append(trainee.getMark()).append(" ");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(test))) {
            bw.write(sb.toString());
        } catch (IOException e) {
            fail();
        }
        try (BufferedReader br = new BufferedReader(new FileReader(test))) {
            temp = br.readLine();
            if (temp != null) {
                String[] values = temp.split(" ");
                try {
                    fromFile = new Trainee(values[0], values[1], Integer.parseInt(values[2]));
                } catch (NumberFormatException | TraineeException e) {
                    fail();
                }
            } else {
                fail("String with data expected");
            }
        } catch (IOException e) {
            fail();
        }
        try (FileChannel channel = new FileInputStream(test).getChannel()) {
            ByteBuffer buffer = ByteBuffer.allocate(11);
            channel.read(buffer);
            assertEquals(new String(buffer.array()), temp);
        } catch (IOException e) {
            fail();
        }

        try (FileChannel channel = new RandomAccessFile(test, "r").getChannel()) {
            MappedByteBuffer buffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, 11);
            String charEncoding = System.getProperty("file.encoding");
            CharBuffer charBuffer = Charset.forName(charEncoding).decode(buffer);
            assertEquals(temp, charBuffer.toString());
            buffer = null;
            System.gc();
        } catch (IOException e) {
            fail();
        }
        test.delete();
        assertEquals(trainee, fromFile);
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(test)))) {
            oos.writeObject(trainee);
        } catch (IOException e) {
            fail();
        }
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(test)))) {
            fromFile = (Trainee) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            fail();
        }
        assertEquals(fromFile, trainee);
        test.delete();
    }

    @Test
    public void byteArrayTest() {
        Trainee test = null;
        Trainee test2 = null;
        byte[] byteArray = null;
        ByteArrayOutputStream baos;
        try {
            test = new Trainee("Dee", "Tar", 4);
        } catch (TraineeException e) {
            fail();
        }
        try (ObjectOutputStream oos = new ObjectOutputStream(baos = new ByteArrayOutputStream(16))) {
            oos.writeObject(test);
            byteArray = baos.toByteArray();
        } catch (IOException e) {
            fail();
        }
        try (ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(byteArray))) {
            test2 = (Trainee) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            fail();
        }
        assertEquals(test, test2);
    }

    @Test
    public void gsonTest() {
        Trainee trainee = null;
        Trainee same;
        File file = new File("trainee.txt");
        try {
            trainee = new Trainee("Den", "Keen", 4);
        } catch (TraineeException e) {
            fail();
        }
        Gson gson = new Gson();
        String gsonText = gson.toJson(trainee);
        try (BufferedWriter bos = new BufferedWriter(new FileWriter(file))) {
            bos.write(gsonText);
        } catch (IOException e) {
            fail();
        }
        String textFromFile = null;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            textFromFile = br.readLine();
        } catch (IOException e) {
            fail();
        }
        if (textFromFile == null)
            fail();
        same = gson.fromJson(textFromFile, Trainee.class);
        assertEquals(trainee, same);
        file.delete();
    }

    @Test
    public void mappedByteBufferTest() {
        File file = new File("array.txt");
        try (FileChannel fc = new RandomAccessFile(file, "rw").getChannel()) {
            MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_WRITE, 0, 100);
            for (int i = 0; i < 100; i++) {
                mbb.put((byte) i);
            }
            assertEquals(mbb.get(77), (byte) 77);
            mbb = null;
            System.gc();
        } catch (IOException e) {
            fail();
        }
        file.delete();
    }

    @Test
    public void pathsAndFiles() {
        Path home = Paths.get(System.getProperty("user.home"));
        try {
            FileStore fs = Files.getFileStore(home);
            assertEquals(fs.isReadOnly(), false);
        } catch (IOException e) {
            fail();
        }
        try {
            Path file = Files.createTempFile(home, "temp", "txt");
            assertEquals(file.getParent(), home);
        } catch (IOException e) {
            fail();
        }
    }
}