package net.thumbtack.tsoy.trainee.utilities;

import net.thumbtack.tsoy.trainee.exceptions.InstituteException;
import net.thumbtack.tsoy.trainee.exceptions.TraineeException;
import net.thumbtack.tsoy.trainee.trainee.Trainee;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static org.junit.Assert.*;


public class InstituteTest {
    @Test
    public void test() {
        Institute omsu = null;
        try {
            omsu = new Institute("OmSU", "");
        } catch (InstituteException e) {
            assertEquals(omsu, null);
        }
    }

    @Test
    public void hashMapTest() {
        Trainee one = null, two = null, three = null;
        try {
            one = new Trainee("Dilan", "Rogers", 5);
            two = new Trainee("Gleb", "Lipatnikov", 2);
            three = new Trainee("Gleb", "Ivanov", 3);
        } catch (TraineeException e) {
            fail();
        }
        Institute omsu = null, nsu = null, msu = null;
        try {
            omsu = new Institute("OmSU", "Omsk");
            nsu = new Institute("NSU", "Novosibirsk");
            msu = new Institute("MSU", "Moscow");
        } catch (InstituteException e) {
            fail();
        }
        Map<Trainee, Institute> map = new HashMap<>(5);
        map.put(one, omsu);
        map.put(two, nsu);
        map.put(three, msu);
        assertEquals(map.get(two), nsu);
        for (Trainee t : map.keySet()) {
            System.out.println(t.getLastName());
        }
    }

    @Test
    public void treeSetTest() {
        Trainee one = null, two = null, three = null;
        try {
            one = new Trainee("Dilan", "Rogers", 5);
            two = new Trainee("Gleb", "Lipatnikov", 2);
            three = new Trainee("Gleb", "Ivanov", 3);
        } catch (TraineeException e) {
            fail();
        }
        Institute omsu = null, nsu = null, msu = null;
        try {
            omsu = new Institute("OmSU", "Omsk");
            nsu = new Institute("NSU", "Novosibirsk");
            msu = new Institute("MSU", "Moscow");
        } catch (InstituteException e) {
            fail();
        }
        SortedMap<Trainee, Institute> map = new TreeMap<>();
        map.put(one, omsu);
        map.put(two, nsu);
        map.put(three, msu);
        assertEquals(map.get(one), omsu);
        //because comparing first names
        assertNotEquals(map.get(two), nsu);
        for (Trainee t : map.keySet()) {
            System.out.println(t.getLastName());
        }
        System.out.println(map.subMap(one, three));
    }
}
