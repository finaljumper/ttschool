package net.thumbtack.tsoy.trainee.utilities;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.*;

public class TypeActionsTest {
    @Test
    public void testString() {
        TypeActions test = new TypeActions();
        String msg1 = test.message;
        String msg2 = test.unformattedMessage;
        String msg3 = test.simpleString;
        String msg = " lol ";
        char[] temp = new char[3];
        assertEquals(msg1.charAt(msg1.length() - 1), 't');
        assertEquals(msg1.indexOf('e') != msg1.lastIndexOf('e'), true);
        assertEquals(msg1.equalsIgnoreCase(msg2) && !(msg1.equals(msg2)), true);
        assertEquals(msg1.compareTo("Message") > 0, true);
        assertEquals(msg1.compareToIgnoreCase("message") > 0, true);
        assertEquals(msg3 + " chat", "simple chat");
        msg3 += "test";
        assertEquals(msg3, "simpletest");
        assertEquals(msg3.concat("."), "simpletest.");
        assertEquals(msg3.startsWith("si"), true);
        assertEquals(msg3.endsWith("t"), true);
        assertEquals(msg3.substring(0, 3), "sim");
        assertEquals(msg3.getBytes()[0], 's');
        msg3.getChars(0, 3, temp, 0);
        assertEquals(temp[2], 'm');
        assertEquals(msg3.replace("test", " "), "simple ");
        assertEquals(msg3.replaceFirst("s", "a"), "aimpletest");
        assertEquals(msg3.split("m")[0], "si");
        assertEquals(msg.trim(), "lol");
        assertEquals(String.valueOf(temp), "sim");
        assertEquals(String.format(new Locale("ru"),"%f", Math.E), "2,718282");
        test.builder.insert(0, "good");
        test.builder.append(" times");
        assertEquals(test.builder.toString(), "good times");
    }
}