package my.thumbtack.rest;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class PostFilter implements ContainerRequestFilter {

    public void filter(ContainerRequestContext requestContext)
            throws IOException {
        System.out.println("POST");
    }
}
