package my.thumbtack.rest.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import my.thumbtack.rest.exception.ResourceDuplicateException;
import my.thumbtack.rest.exception.ResourceNotFoundException;
import my.thumbtack.rest.model.todo.TodoList;
import my.thumbtack.rest.model.todo.TodoTask;
import my.thumbtack.rest.model.todo.exception.TodoTaskDuplicateException;
import my.thumbtack.rest.model.todo.exception.TodoTaskNotFoundException;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/tasks")
public class TodoListResource {
    private static TodoList todoList = new TodoList();
    final Gson gson = new GsonBuilder().create();

    @GET
    @Produces("application/json")
    public Response getAllTasks() {
        return Response.ok(gson.toJson(todoList.getAllTasks())).build();
    }

    @POST
    @Consumes("application/json")
    public Response addTask(String body) {
        try {
            todoList.addTask(gson.fromJson(body, TodoTask.class));
            return Response.ok().build();
        } catch (TodoTaskDuplicateException e) {
            throw new ResourceDuplicateException();
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    public Response editTask(@PathParam("id") String id, String body) {
        try {
            todoList.editTask(id, gson.fromJson(body, TodoTask.class));
            return Response.ok().build();
        } catch (TodoTaskNotFoundException e) {
            throw new ResourceNotFoundException();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response deleteTask(@PathParam("id") String id) {
        try {
            todoList.deleteTask(id);
            return Response.ok().build();
        } catch (TodoTaskNotFoundException e) {
            throw new ResourceNotFoundException();
        }
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getTask(@PathParam("id") String id) {
        try {
            return Response.ok(gson.toJson(todoList.getTask(id))).build();
        } catch (TodoTaskNotFoundException e) {
            throw new ResourceNotFoundException();
        }
    }

    @DELETE
    public Response deleteAllTasks() {
        todoList.deleteAllTasks();
        return Response.ok().build();
    }
}
