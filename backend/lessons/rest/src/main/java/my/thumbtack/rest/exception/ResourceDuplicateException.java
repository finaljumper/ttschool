package my.thumbtack.rest.exception;


import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ResourceDuplicateException extends WebApplicationException {
    @Override
    public Response getResponse() {
        return Response.serverError()
                .entity("{\"message\": \"RESOURCE ALREADY EXISTS\"}")
                .header("Content-Type", MediaType.APPLICATION_JSON_TYPE)
                .build();
    }
}
