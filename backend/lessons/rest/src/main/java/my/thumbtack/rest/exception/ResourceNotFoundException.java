package my.thumbtack.rest.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ResourceNotFoundException extends WebApplicationException {

    @Override
    public Response getResponse() {
        return Response.status(404)
            .entity("{\"message\": \"RESOURCE NOT FOUND\"}")
            .header("Content-Type", MediaType.APPLICATION_JSON_TYPE)
            .build();
    }
}
