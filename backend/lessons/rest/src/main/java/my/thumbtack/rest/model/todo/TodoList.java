package my.thumbtack.rest.model.todo;

import my.thumbtack.rest.model.todo.exception.TodoTaskDuplicateException;
import my.thumbtack.rest.model.todo.exception.TodoTaskNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class TodoList {

    private static List<TodoTask> todoTasks = new ArrayList<>();

    public void addTask(TodoTask task) throws TodoTaskDuplicateException {
        task.generateId();
        for (TodoTask element : todoTasks) {
            if (element.getId().equals(task.getId()))
                throw new TodoTaskDuplicateException();
        }
        todoTasks.add(task);
    }

    public TodoTask getTask(String id) throws TodoTaskNotFoundException {
        for (TodoTask task : todoTasks) {
            if (task.getId().equals(id))
                return task;
        }
        throw new TodoTaskNotFoundException();
    }

    public void deleteTask(String id) throws TodoTaskNotFoundException {
        for (TodoTask task : todoTasks) {
            if (task.getId().equals(id)){
                todoTasks.remove(task);
                return;
            }
        }
        throw new TodoTaskNotFoundException();
    }

    public void editTask(String id, TodoTask editedTask) throws TodoTaskNotFoundException {
        for (TodoTask task : todoTasks) {
            if (task.getId().equals(id)){
                task.setText(editedTask.getText());
                task.setType(editedTask.getType());
                task.setStatus(editedTask.isStatus());
                return;
            }
        }
        throw new TodoTaskNotFoundException();
    }

    public void deleteAllTasks() {
        todoTasks.clear();
    }

    public List<TodoTask> getAllTasks() {
        return todoTasks;
    }

}
