package my.thumbtack.rest.model.todo;

import java.util.UUID;

public class TodoTask {
    private String text;
    private String id;
    private TodoType type;
    private boolean status;

    public TodoTask(String text, TodoType type) {
        this.text = text;
        this.type = type;
        this.status = false;
        id = UUID.randomUUID().toString();
    }

    public void generateId() {
        id = UUID.randomUUID().toString();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public TodoType getType() {
        return type;
    }

    public void setType(TodoType type) {
        this.type = type;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TodoTask todoTask = (TodoTask) o;

        if (!text.equals(todoTask.text)) return false;
        if (!id.equals(todoTask.id)) return false;
        return type == todoTask.type;
    }

    @Override
    public int hashCode() {
        int result = text.hashCode();
        result = 31 * result + id.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }
}
