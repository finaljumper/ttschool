package my.thumbtack.rest.model.todo;

public enum TodoType {
    NORMAL, URGENT
}
