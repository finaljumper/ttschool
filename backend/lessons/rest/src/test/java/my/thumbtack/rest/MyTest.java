package my.thumbtack.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.internal.RestAssuredResponseImpl;
import io.restassured.internal.mapping.GsonMapper;
import io.restassured.mapper.ObjectMapper;
import io.restassured.mapper.ObjectMapperDeserializationContext;
import io.restassured.mapper.ObjectMapperSerializationContext;
import io.restassured.parsing.Parser;
import my.thumbtack.rest.model.todo.TodoTask;
import my.thumbtack.rest.model.todo.TodoType;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import javax.ws.rs.core.Application;

import java.util.UUID;

import static io.restassured.RestAssured.*;

public class MyTest extends JerseyTest {
    private Gson gson = new GsonBuilder().create();
    private TodoTask[] tasks;

    @Override
    protected Application configure() {
        System.out.println("HI");
        return new MyApplication();
    }


    @Test
    public void addTaskTest() {
        RestAssured.defaultParser = Parser.JSON;
        given()
                .contentType("application/json")
                .body(gson.toJson(new TodoTask("eat", TodoType.NORMAL)))
                .when()
                .post("http://localhost:9998/tasks")
                .then()
                .statusCode(200);

        given()
                .contentType("application/json")
                .body("{\"text\": \"sleep\",\"id\": 0,\"type\": \"URGENT\",\"status\": \"false\"}")
                .when()
                .post("http://localhost:9998/tasks")
                .then()
                .statusCode(200);
    }

    @Test
    public void getTaskTest() {
        tasks = given()
                .when()
                .get("http://localhost:9998/tasks")
                .then()
                .statusCode(200)
                .extract()
                .as(TodoTask[].class);
        Assert.assertEquals(tasks.length, 2);
        TodoTask taskToCheck = given()
                .when()
                .get("http://localhost:9998/tasks/" + tasks[0].getId())
                .then()
                .statusCode(200)
                .extract()
                .as(TodoTask.class);
        Assert.assertEquals(taskToCheck.getText(), tasks[0].getText());
    }

    @Test
    public void editTaskTest() {
        addTaskTest();
        tasks = given()
                .when()
                .get("http://localhost:9998/tasks")
                .then()
                .statusCode(200)
                .extract()
                .as(TodoTask[].class);
        given()
                .contentType("application/json")
                .body("{\"text\": \"find book\",\"id\": 0,\"type\": \"NORMAL\"}")
                .when()
                .put("http://localhost:9998/tasks/" + tasks[1].getId())
                .then()
                .statusCode(200);
        TodoTask taskToEdit = given()
                .when()
                .get("http://localhost:9998/tasks/" + tasks[1].getId())
                .then()
                .statusCode(200)
                .extract()
                .as(TodoTask.class);
        Assert.assertEquals(taskToEdit.getText(), "find book");
        Assert.assertEquals(taskToEdit.getType(), TodoType.NORMAL);
    }

    @Test
    public void deleteTaskTest() {
        addTaskTest();
        tasks = given()
                .when()
                .get("http://localhost:9998/tasks")
                .then()
                .statusCode(200)
                .extract()
                .as(TodoTask[].class);

        given()
                .when()
                .delete("http://localhost:9998/tasks/" + tasks[1].getId())
                .then()
                .statusCode(200);

        given()
                .when()
                .get("http://localhost:9998/tasks/" + tasks[1].getId())
                .then()
                .statusCode(404);

        given()
                .when()
                .delete("http://localhost:9998/tasks/")
                .then()
                .statusCode(200);

        tasks = given()
                .when()
                .get("http://localhost:9998/tasks")
                .then()
                .statusCode(200)
                .extract()
                .as(TodoTask[].class);

        Assert.assertEquals(tasks.length, 0);
    }

}
