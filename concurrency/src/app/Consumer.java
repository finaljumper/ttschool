package app;

import java.util.List;
import java.util.Random;

/**
 * Created by renai on 19.02.17.
 */
public class Consumer implements Runnable {

    private final Random random = new Random();
    private List<Integer> list;
    public Thread thread;

    Consumer(List<Integer> list) {
        this.list = list;
        thread = new Thread(this, "Consumer");
        thread.start();
    }

    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            list.remove(random.nextInt(list.size()));
        }
        System.out.println("Deleted elements " + list.size());
        System.out.println(Thread.currentThread());
    }
}
