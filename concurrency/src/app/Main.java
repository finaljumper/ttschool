package app;

import app.thread.CurrentThread;
import app.thread.Thread1;
import app.thread.Thread2;
import app.thread.Thread3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

public class Main {

    public static void main(String[] args) {
//        CurrentThread.printInfo();
//        threadJoin();
//        externalSync();
//        internalSync();
//        pingPong();
        collectionSync();
    }

    public static void threadJoin() {
        try {
            new Thread1().join();
            new Thread2().join();
            new Thread3().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Main thread");
    }

    public static void externalSync() {
        final List<Integer> list = new ArrayList<>();
        try {
            synchronized (list) {
                Producer producer = new Producer(list);
                producer.thread.join();
                Consumer consumer = new Consumer(list);
                consumer.thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void reentrantLockExternalSync() {
        final List<Integer> list = new ArrayList<>();
        ReentrantLock lock = new ReentrantLock();
        try {
            synchronized (list) {
                Producer producer = new Producer(list);
                producer.thread.join();
                Consumer consumer = new Consumer(list);
                consumer.thread.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void internalSync() {
        List<Integer> list = new ArrayList<>();
        try {
            new ThreadSafeModifier(list, true).t.join();
            new ThreadSafeModifier(list, false).t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(list.size());
    }

    public static void collectionSync() {
        List<Integer> list = new ArrayList<>();
        List<Integer> syncList = Collections.synchronizedList(list);
        try {
            Producer producer = new Producer(syncList);
            producer.thread.join();
            Consumer consumer = new Consumer(syncList);
            consumer.thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void pingPong() {
        Semaphore semaphore = new Semaphore(1);
        Ping ping = new Ping(semaphore);
        Pong pong = new Pong(semaphore);
    }
}
