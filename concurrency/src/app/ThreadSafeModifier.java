package app;

import java.util.List;
import java.util.Random;

/**
 * Created by renai on 19.02.17.
 */
public class ThreadSafeModifier implements Runnable {

    private List<Integer> list;
    private Random random = new Random();
    private boolean option;
    public Thread t;

    ThreadSafeModifier(List<Integer> list, boolean option) {
        super();
        this.list = list;
        this.option = option;
        t = new Thread(this);
        t.start();
    }

    synchronized public void add() {
        for (int i = 0; i < 10000; i++) {
            list.add(random.nextInt());
        }
        System.out.println(Thread.currentThread());
    }

    synchronized public void delete() {
        for (int i = 0; i < 10000; i++) {
            list.remove(random.nextInt(list.size()));
        }
        System.out.println(Thread.currentThread());
    }

    @Override
    public void run() {
        if (option)
            this.add();
        else
            this.delete();
    }
}
