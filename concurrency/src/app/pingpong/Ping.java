package app;

import java.util.concurrent.Semaphore;

/**
 * Created by renai on 19.02.17.
 */
public class Ping implements Runnable {

    private Semaphore semaphore;

    Ping(Semaphore semaphore) {
        this.semaphore = semaphore;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while(true) {
            try {
                semaphore.acquire();
                System.out.println("Ping");
                semaphore.release();
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
