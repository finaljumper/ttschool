package app;

import java.util.concurrent.Semaphore;

/**
 * Created by renai on 20.02.17.
 */
public class Pong implements Runnable {
    private Semaphore semaphore;

    Pong(Semaphore semaphore) {
        this.semaphore = semaphore;
        new Thread(this).start();
    }

    @Override
    public void run() {
        while(true) {
            try {
                semaphore.acquire();
                System.out.println("Pong");
                semaphore.release();
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
