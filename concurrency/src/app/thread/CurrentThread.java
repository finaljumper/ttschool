package app.thread;

/**
 * Created by renai on 19.02.17.
 */
public class CurrentThread {
    public static void printInfo() {
        Thread t = Thread.currentThread();
        System.out.println("Current thread: " + t);
    }
}
